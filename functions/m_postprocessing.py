"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    # Betrag berechnen 
    vector = np.array([x, y, z])
    return np.linalg.norm(vector, axis=0)


def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    
    interpolator = np.interp
    
    # gleichmäßig verteilte Zeitpunkte für die Interpolation erstellen 
    equidistant_time_points = np.linspace(min(time), max(time), len(time))
    interpolated_data = interpolator(equidistant_time_points, time, data) #Interpolation
    return equidistant_time_points, interpolated_data # Daten ausgeben lassen


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    x -= np.mean(x) # Mittelwert bei 0
    
    # Berechnen der FFT
    fft_result = fft(x)
    
    # Entsprechende Frequenzen berechnen
    sampling_rate = 1 / (time[1] - time[0])
    frequencies = np.fft.fftfreq(len(x), d=1/sampling_rate)
    
    # Nur positive Frequenzen auswählen 
    positive_frequencies_mask = frequencies >= 0
    
    # Ausgabe nur von positiven Frequenzen
    return np.abs(fft_result[positive_frequencies_mask]), frequencies[positive_frequencies_mask]
    
     
     